## 0.1.3

Fix wire datatype issue

## 0.1.2

Add optional parameter for error throttling interval to prevent multiple pop ups from overwhelming user.

## 0.1.1

Updated Readme and Relinked VIs

## 0.1.0

Initial Release.
